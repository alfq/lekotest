import { UserService } from "./user.service";
import { Component } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { NicknameValidatorService } from "./validators/nickname.validator.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  form;
  users = [];

  constructor(
    fb: FormBuilder,
    private userService: UserService,
    private nicknameService: NicknameValidatorService
  ) {
    this.form = fb.group({
      nickname: [
        "",
        [Validators.required, Validators.pattern("([A-Za-z][a-zA-Z0-9]*s*)+")],
        nicknameService.checkNickname.bind(nicknameService)
      ],
      firstname: [
        "",
        [Validators.required, Validators.pattern("^[\u0400-\u04FF]*$")]
      ],
      surname: [
        "",
        [Validators.required, Validators.pattern("^[\u0400-\u04FF]*$")]
      ],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(5)]]
    });
  }

  get nickname() {
    return this.form.get("nickname");
  }

  get firstname() {
    return this.form.get("firstname");
  }

  get surname() {
    return this.form.get("surname");
  }

  get email() {
    return this.form.get("email");
  }

  get password() {
    return this.form.get("password");
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(users => {
      return (this.users = users);
    });
  }

  submit(input) {
    if(this.form.valid){
      this.userService.createUser(input).subscribe(data => {
        // console.log(data);
      });
      this.form.reset();
      alert("Successfully Submitted!")
    }
  }
}
