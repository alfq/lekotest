import { TestBed, inject } from '@angular/core/testing';

import { Nickname.ValidatorService } from './nickname.validator.service';

describe('Nickname.ValidatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Nickname.ValidatorService]
    });
  });

  it('should be created', inject([Nickname.ValidatorService], (service: Nickname.ValidatorService) => {
    expect(service).toBeTruthy();
  }));
});
