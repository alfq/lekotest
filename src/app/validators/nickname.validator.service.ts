import { apiURL } from "./../config";
import { Observable } from "rxjs/Observable";
import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { FormControl } from "@angular/forms";

@Injectable()
export class NicknameValidatorService {
  debouncer: any;

  constructor(private http: Http) {}

  validateUsername(nickname) {
    return this.http
      .get(`${apiURL + nickname}`)
      .map(res => JSON.parse(JSON.stringify(res || null)));
  }

  checkNickname(control: FormControl): any {
    clearTimeout(this.debouncer);

    return new Promise(resolve => {
      this.debouncer = setTimeout(() => {
        this.validateUsername(control.value).subscribe(
          res => {
            if (res.ok) {
              // console.log("Taken!!");
              resolve({ nicknameInUse: true });
            }
          },
          err => {
            // console.log(err);
            // console.log("Ok!");
            resolve(null);
          }
        );
      }, 1000);
    });
  }
}
