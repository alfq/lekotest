import { apiURL } from './config';
import { Injectable } from '@angular/core';
import { Http, Response,Headers, RequestOptions } from '@angular/http';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(private httpService: Http) { }

  getUsers(): Observable<any>{
    return this.httpService.get(apiURL + "getusers")
      .map((response: Response)=> <any>response.json())
      .catch(this.handleError)
  }

  createUser(data) : Observable<any> {
    let body = JSON.stringify(data);
    let headers = new Headers({"Content-Type": "application/json"})
    let options = new RequestOptions({headers:headers})
    return this.httpService.post(apiURL+"create/",body,options)
      .map((response: Response) => <any>response.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
