import { NicknameValidatorService } from "./validators/nickname.validator.service";
import { UserService } from "./user.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, HttpModule],
  providers: [UserService, NicknameValidatorService],
  bootstrap: [AppComponent]
})
export class AppModule {}
