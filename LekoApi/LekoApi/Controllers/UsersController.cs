﻿using LekoApi.Models;
using LekoApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;

namespace LekoApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllHeaders")]
    public class UsersController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet("GetUsers")]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _userRepository.GetAll();
            return Ok(users);
        }

        [HttpGet("{nickname}", Name = "GetUser")]
        public async Task<IActionResult> GetByNickname(string nickname)
        {
            var user = await _userRepository.GetByNickname(nickname);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(nickname);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            await _userRepository.Add(user);
            return CreatedAtRoute("GetUser", new {Controller = "Users", nickname = user.Nickname}, user);
        }
    }
}