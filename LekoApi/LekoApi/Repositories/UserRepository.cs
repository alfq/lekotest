﻿using LekoApi.Data;
using LekoApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LekoApi.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Add(User user)
        {
           await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> GetByNickname(string nickname)
        {
            return await _context.Users.SingleOrDefaultAsync(c => c.Nickname == nickname);
        }
    }
}