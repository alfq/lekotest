﻿using LekoApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LekoApi.Repositories
{
    public interface IUserRepository
    {
        Task Add(User user);
        Task<User> GetByNickname(string nickname);
        Task<IEnumerable<User>> GetAll();
    }
}